import styled from "styled-components";


export const TextInput = styled.input`
    border: none;
    outline: none;
    width: 100%;
    background: rgba(0, 0, 0, 0.32);
    backdrop-filter: blur(2px);
    border-radius: 4px;
    font-size: 16px;
    line-height: 24px;
    padding-left: 16px;
    padding-top: 16px;
    height: 60px; 
    color: var(--white);
`;

export const RightAddon = styled.span`
    position: absolute;
    right: 16px;
    top: 20%;
`


export const Label = styled.label`
    color: var(--white);
    top: 8px;
    left: 16px;
    position: absolute;
    z-index: 4;
    opacity: .5;
    font-size: 12px;
`

export const Container = styled.div`
    position: relative;
    width: 100%;
    height: 60px;
`;