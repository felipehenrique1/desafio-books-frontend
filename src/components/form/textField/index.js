import React from 'react'
import { Container, TextInput, Label, RightAddon } from './textField.styles';

function TextField({
    label,
    type,
    buttonAddon: Button,
    ...props
}) {
    return (
        <Container>
            <Label>{label}</Label>
            <TextInput  type={type} {...props}/>
            {Button && (<RightAddon>
                {Button}

            </RightAddon>
            )}
        </Container>
    )
}

export default TextField;