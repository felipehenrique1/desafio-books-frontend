import styled from "styled-components";

export const BoxContainer = styled.div`
    width: ${props => props.width ?? 'auto'};
    max-width: ${props => props.maxWidth ?? 'auto'};
    height: ${props => props.height ?? 'auto'};
    display: ${props => props.display ?? 'block'};
    padding: ${props => props.padding ?? 'inherit'};
    padding-top: ${props => props.paddingTop ?? 'inherit'};
`