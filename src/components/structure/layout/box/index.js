import React from 'react'
import { BoxContainer } from './box.styles'

export default function Box({ children, ...props}) {
    return (
        <BoxContainer {...props}>
            {children}
        </BoxContainer>
    )
}
