import React from 'react'
import styled from 'styled-components'

const LayoutOneColumn = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    padding-left: 80px;
    align-items: center;

    @media (max-width: 768px) {
        padding-left: 0px;
        padding: 10px;
    }
`

export default LayoutOneColumn;
