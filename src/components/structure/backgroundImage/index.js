import PropTypes from "prop-types";
import { Container } from "./BackgroundImage.styles";

function BackgroundImage({
    src,
    children
}) {
    return (
    <Container src={src}>
        {children}
    </Container>
    );
}

BackgroundImage.propTypes = {
    src: PropTypes.string.isRequired,
}

export default BackgroundImage;