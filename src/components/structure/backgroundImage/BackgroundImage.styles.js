import styled from "styled-components";

export const Container = styled.div`
    background-image: url(${props => props.src});
    width: 100%;
    height: 100vh;
    background-size: cover;

    @media (max-width: 1024px) {
        background-size: cover;
        background-position: center center;
    }

`;
