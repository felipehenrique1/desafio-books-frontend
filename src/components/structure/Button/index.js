import styled from "styled-components";


export const Button = styled.button`
    border: none;
    background-color: ${props => props.bgColor};
    color: ${props => props.color};
    border-radius: 44px;
    padding: 8px 21px;
    font-weight: 500;
    font-size: 16px;
`;