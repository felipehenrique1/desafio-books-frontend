import React from 'react'
import TextField from '../../components/form/textField'
import { Button } from '../../components/structure/Button'
import Box from '../../components/structure/layout/box'

export default function HomeForm() {
    return (
        <form>
            <TextField type="email" label="Email" />
            <Box paddingTop="20px">
            </Box>
            <TextField 
                type="password"
                buttonAddon={(
                    <Button bgColor="#fff" color="var(--color-primary)">
                        Entrar
                    </Button>
                )} 
                label="Password" />
        </form>
    )
}
