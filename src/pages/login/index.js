import React from 'react'
import BackgroundImage from '../../components/structure/backgroundImage';
import Box from '../../components/structure/layout/box';
import LoginBackground from '../../assets/images/LoginBackground.png';
import LayoutOneColumn from '../../components/structure/layout/oneColumn';
import Header from '../../assets/images/Header.svg';
import HomeForm from './form';

export default function Login() {
    return (
        <BackgroundImage src={LoginBackground}>
            <LayoutOneColumn>
                <Box maxWidth="459px" width="100%">
                    <img src={Header} width="198px" height="40px"/> 
                    <Box padding="20px" />
                    <HomeForm />
                </Box>
            </LayoutOneColumn>
        </BackgroundImage>
    )
}
